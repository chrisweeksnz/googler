# Overview

Container to deploy googler.

The googler project can be found here:  https://github.com/jarun/googler

This is merely packaging the authors excellent work.


### Usage

```shell
function googler(){
  docker run -it --rm registry.gitlab.com/chrisweeksnz/googler:latest $@
}
```