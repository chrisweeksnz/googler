FROM alpine:latest
LABEL maintainer="Chris Weeks <chris@weeks.net.nz>" \
      project-site="https://github.com/jarun/googler" \
      build-command="docker build -t googler ."
RUN apk update && \
    apk add curl python3 && \
    googler_version=$(curl -s https://github.com/jarun/googler/releases/latest | sed -e 's|">redirected.*||' -e 's|.*/||') && \
    curl -o /usr/local/bin/googler \
         https://raw.githubusercontent.com/jarun/googler/${googler_version}/googler && \
    chmod +x /usr/local/bin/googler
ENTRYPOINT ["/usr/local/bin/googler"]
